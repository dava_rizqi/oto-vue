const path = require("path");
module.exports = {
  transpileDependencies: [`vuetify`],

  lintOnSave: false,
  outputDir: path.resolve(__dirname, "../www/"),

  configureWebpack: (config) => {
    if (process.env.NODE_ENV === `production`) {
      config.output.publicPath = ``;
    }
  },

  // publicPath: process.env.NODE_ENV === `production` ? `` : `/`,
  publicPath: "",

  pluginOptions: {
    cordovaPath: "src-cordova",
  },
};
