import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-default.css";
import VuePageTransition from "vue-page-transition";
import axios from "axios"; //nembak api

Vue.use(VueToast);
Vue.use(VuePageTransition);
Vue.config.productionTip = false;

// axios.defaults.baseURL = "http://localhost:8000/api/";
axios.defaults.baseURL = "https://oto.davarp.id/api/";

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
