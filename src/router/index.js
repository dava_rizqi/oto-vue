import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Splash from "../views/Splash.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Splash",
    meta: { navbar: false, back: false },
    component: Splash,
  },
  {
    path: "/home",
    name: "Home",
    meta: { navbar: true, back: false },
    component: Home,
  },
  {
    path: "/invoice",
    name: "Invoice",
    meta: { navbar: false, back: true, hide: true, prevLink: "before" },
    component: () => import("../views/Invoice.vue"),
  },
  {
    path: "/workshop",
    name: "Workshop",
    meta: { navbar: false, back: true, prevLink: "/home" },
    component: () => import("../views/Workshop.vue"),
  },
  {
    path: "/workshop-profile",
    name: "WorkshopProfile",
    meta: { navbar: false, back: true, prevLink: "/workshop" },
    component: () => import("../views/WorkshopProfile.vue"),
  },
  {
    path: "/login",
    name: "Login",
    meta: { navbar: false, back: false },
    component: () => import("../views/Auth/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    meta: { navbar: false, back: false },
    component: () => import("../views/Auth/Register.vue"),
  },
  {
    path: "/complete-role",
    name: "CompleteRole",
    meta: { navbar: false, back: false },
    component: () => import("../views/Auth/Role.vue"),
  },
  {
    path: "/complete-question",
    name: "CompleteQuestion",
    meta: { navbar: false, back: false },
    component: () => import("../views/Auth/Question.vue"),
  },
  {
    path: "/complete-profile",
    name: "CompleteProfile",
    meta: { navbar: false, back: false },
    component: () => import("../views/Auth/Profile.vue"),
  },
  {
    path: "/order",
    name: "Order",
    meta: { navbar: false, back: true, prevLink: "/home" },
    component: () => import("../views/Order.vue"),
  },
  {
    path: "/profile",
    name: "Profile",
    meta: { navbar: false, back: true, prevLink: "/home" },
    component: () => import("../views/Profile.vue"),
  },
  {
    path: "/edit-profile",
    name: "EditProfile",
    meta: { navbar: false, back: true, prevLink: "/profile" },
    component: () => import("../views/edit-profile.vue"),
  },
  {
    path: "/setting",
    name: "Setting",
    meta: { navbar: true, back: false },
    component: () => import("../views/Setting.vue"),
  },
  {
    path: "/history",
    name: "History",
    meta: { navbar: true, back: false },
    component: () => import("../views/History.vue"),
  },
  {
    path: "/queue",
    name: "Queue",
    meta: { navbar: true, back: false },
    component: () => import("../views/Queue.vue"),
  },
  {
    path: "/service",
    name: "Service",
    meta: { navbar: true, back: false },
    component: () => import("../views/Service.vue"),
  },
  {
    path: "/create-service",
    name: "CreateService",
    meta: { navbar: false, back: true, prevLink: "/home" },
    component: () => import("../views/workshop/create-service.vue"),
  },
  {
    path: "/create-order",
    name: "CreateOrder",
    meta: { navbar: false, back: true, prevLink: "/home" },
    component: () => import("../views/workshop/create-order.vue"),
  },
  {
    path: "/create-order-customer",
    name: "CreateOrderCustomer",
    meta: { navbar: false, back: true, prevLink: "before" },
    component: () => import("../views/CreateOrder.vue"),
  },
  {
    path: "/view-service",
    name: "ViewService",
    meta: { navbar: false, back: true, prevLink: "/profile" },
    component: () => import("../views/workshop/view-service.vue"),
  },
  {
    path: "/view-category-service",
    name: "ViewCategoryService",
    meta: { navbar: false, back: true, prevLink: "/view-service" },
    component: () => import("../views/workshop/view-category-service.vue"),
  },
  {
    path: "/edit-service",
    name: "EditService",
    meta: { navbar: false, back: true, prevLink: "/detail-service" },
    component: () => import("../views/workshop/edit-service.vue"),
  },
  {
    path: "/detail-service",
    name: "DetailService",
    meta: { navbar: false, back: true, prevLink: "/view-service" },
    component: () => import("../views/detail-service.vue"),
  },
  {
    path: "/verification",
    name: "Verification",
    meta: { navbar: false, back: false },
    component: () => import("../views/verification.vue"),
  },
  { path: "*", redirect: "/" },
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes,
});

export default router;
