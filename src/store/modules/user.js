import Vue from "vue";
import axios from "axios";
import router from "../../router/index";

const Auth = {
  namespaced: true,
  state() {
    return {
      user: {
        role_id: 0,
        kota: null,
        alamat: null,
        nama: null,
        nohp: null,
        is_verified: null,
        skor: null,
      },
      users: [],
    };
  },
  mutations: {
    async GET_PROFILE(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      await axios
        .post("users/detail", LOGGED_IN_USER)
        .then(async (res) => {
          var user = res.data;
          localStorage.setItem("oto_session", JSON.stringify(user));
          state.user = res.data;
          console.log(res.data);

          if (user.role_id == 0) {
            router.push({ name: "CompleteRole" });
          }
          if (user.role_id == 2) {
            await db.ref("pelanggan/" + user.id).update(user);
          } else {
            await db.ref("bengkel/" + user.id).update(user);
          }
          if (user.skor != 0) {
            if (user.nama && user.kota && user.alamat && user.nohp) {
              if (user.is_verified) {
                router.push({ name: "Home" }).catch((err) => {});
              } else {
                router.push({ name: "Verification" }).catch((err) => {});
              }
            } else {
              router.push({ name: "CompleteProfile" }).catch((err) => {});
            }
          } else {
            router.push({ name: "CompleteQuestion" }).catch((err) => {});
          }
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    CLEAR_USER(state, payload) {
      state.user = {
        role_id: 0,
        kota: null,
        alamat: null,
        nama: null,
        nohp: null,
      };
      localStorage.removeItem("user_detail");
    },
    USER_DETAIL(state, payload) {
      console.log(payload);
      axios
        .post("users/detail", payload)
        .then((res) => {
          state.user = res.data;
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    GET_USERS(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("users/all", LOGGED_IN_USER)
        .then((res) => {
          state.users = res.data;
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    SEARCH_USERS(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("users/search", {
          role_id: LOGGED_IN_USER.role_id,
          keyword: payload,
        })
        .then((res) => {
          state.users = res.data;
        })
        .catch((err) => {
          console.log(err.response.data);
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    CLEAR_USER(state, payload) {
      state.user = {
        role_id: 0,
        kota: null,
        alamat: null,
        nama: null,
        nohp: null,
      };
    },
  },
  actions: {
    update_profile({ commit }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("users/edit-profile", {
          old_email: LOGGED_IN_USER.email,
          role_id: LOGGED_IN_USER.role_id,
          email: payload.email,
          kota: payload.kota,
          alamat: payload.alamat,
          nohp: payload.nohp,
          password: payload.password,
          jam_buka: payload.jam_buka,
          jam_tutup: payload.jam_tutup,
          nama: payload.nama,
        })
        .then((res) => {
          commit("GET_PROFILE");
          Vue.$toast.open({
            message: "Update profile successfully",
            type: "success",
            position: "bottom",
          });
          router.push("/profile");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
  },
  getters: {
    user(state) {
      return state.user;
    },
    users(state) {
      return state.users;
    },
  },
};

export default Auth;
