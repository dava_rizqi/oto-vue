const Loader = {
  state() {
    return {
      page_loader: true,
    };
  },
  mutations: {
    SET_LOADER(state, payload) {
      state.page_loader = payload;
    },
  },
  getters: {
    page_loader(state) {
      return state.page_loader;
    },
  },
};

export default Loader;
