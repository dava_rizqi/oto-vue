import Vue from "vue";
import axios from "axios";
import router from "../../router/index";
const Auth = {
  state() {
    return {
      oto_session: localStorage.getItem("oto_session")
        ? JSON.parse(localStorage.getItem("oto_session"))
        : null,
    };
  },
  mutations: {
    LOGIN(state, payload) {
      axios
        .post("users/login", payload)
        .then((res) => {
          state.oto_session = localStorage.setItem(
            "oto_session",
            JSON.stringify(res.data)
          );
          Vue.$toast.open({
            message: "Selamat Datang",
            type: "success",
            position: "bottom",
          });
          router.push("/home");
        })
        .catch((err) => {
          console.log(err);
          console.log(err.response);
          Vue.$toast.open({
            message: err.response,
            type: "warning",
            position: "bottom",
          });
        });
    },
  },
  actions: {
    REGISTER({ commit, state }, payload) {
      axios
        .post("users/register", payload)
        .then((res) => {
          localStorage.setItem("oto_session", JSON.stringify(res.data));
          state.oto_session = JSON.parse(localStorage.getItem("oto_session"));
          Vue.$toast.open({
            message: "Anda Berhasil membuat akun",
            type: "success",
            position: "bottom",
          });
          Vue.$toast.open({
            message: "Silahkan Pilih Jenis Akun Anda",
            type: "success",
            position: "bottom",
          });
          router.push("/complete-role");
        })
        .catch((err) => {
          console.log(err.response.data.message);
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    LOGOUT_USER({ commit, state }, payload) {
      localStorage.removeItem("oto_session");
      state.oto_session = null;
      commit("User/CLEAR_USER");
      Vue.$toast.open({
        message: "Sampai Jumpa :)",
        type: "success",
        position: "bottom",
      });
      router.push("/");
    },
    COMPLETE_ROLE({ commit, state }, payload) {
      console.log(state.oto_session);
      axios
        .post("users/complete-role", {
          email: state.oto_session.email,
          role_id: payload,
        })
        .then((res) => {
          state.oto_session = localStorage.setItem(
            "oto_session",
            JSON.stringify(res.data)
          );
          Vue.$toast.open({
            message: "Anda Berhasil memilih role",
            type: "success",
            position: "bottom",
          });
          Vue.$toast.open({
            message: "Silahkan lengkapi profile anda",
            type: "success",
            position: "bottom",
          });
          router.push("/complete-profile");
        })
        .catch((err) => {
          console.log(err.response.data.message);
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    COMPLETE_PROFILE({ commit, state }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("users/edit-profile", {
          old_email: LOGGED_IN_USER.email,
          email: payload.email,
          kota: payload.kota,
          alamat: payload.alamat,
          nohp: payload.nohp,
          password: payload.password,
          nama: payload.nama,
        })
        .then((res) => {
          state.oto_session = localStorage.setItem(
            "oto_session",
            JSON.stringify(res.data)
          );
          commit("User/GET_PROFILE");
          Vue.$toast.open({
            message: "Update profile successfully",
            type: "success",
            position: "bottom",
          });
          router.push("/home");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    COMPLETE_QUESTION({ commit, state }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("users/complete-question", {
          email: LOGGED_IN_USER.email,
          skor: payload.skor,
          isVerified: payload.isVerified,
        })
        .then((res) => {
          state.oto_session = localStorage.setItem(
            "oto_session",
            JSON.stringify(res.data)
          );
          commit("User/GET_PROFILE");
          Vue.$toast.open({
            message: "Update profile successfully",
            type: "success",
            position: "bottom",
          });
          router.push("/complete-profile");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
  },
  getters: {
    oto_session(state) {
      return state.oto_session;
    },
  },
};

export default Auth;
