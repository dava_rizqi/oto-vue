import Vue from 'vue'

const Alert = {
    state () {
        return {

        }
    },
    actions : {
        INFO_ALERT ({commit, state}, payload) {
            console.log(payload)
            Vue.$toast.open({
                message : payload,
                type : 'info',
                position : 'bottom'
            });
        },
        WARNING_ALERT ({commit, state}, payload) {
            console.log(payload)
            Vue.$toast.open({
                message : payload,
                type : 'warning',
                position : 'bottom'
            });
        },
        SUCCESS_ALERT ({commit, state}, payload) {
            console.log(payload)
            Vue.$toast.open({
                message : payload,
                type : 'success',
                position : 'bottom'
            });
        },
        ERROR_ALERT ({commit, state}, payload) {
            console.log(payload)
            Vue.$toast.open({
                message : payload,
                type : 'error',
                position : 'bottom'
            });
        }
    },
    mutations : {

    },
}


export default Alert