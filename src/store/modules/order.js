import Vue from "vue";
import axios from "axios";
import router from "../../router/index";
const Order = {
  state() {
    return {
      orders: [],
      order: null,
      queues: [],
    };
  },
  mutations: {
    CLEAR_ORDER(state, payload) {
      state.orders = [];
      state.order = null;
    },
    GET_QUEUE(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/filter/status", {
          id: LOGGED_IN_USER.id,
          status: "dikerjakan",
          role_id: LOGGED_IN_USER.role_id,
        })
        .then((res) => {
          state.queues = res.data;
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    GET_DETAIL_ORDER(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));

      axios
        .post("orders/detail", {
          id: payload,
          role_id: LOGGED_IN_USER.role_id,
        })
        .then((res) => {
          state.order = res.data;
          console.log(state.order);
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    GET_ORDER(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/all", LOGGED_IN_USER)
        .then((res) => {
          var order = res.data;
          state.orders = res.data;
          if (LOGGED_IN_USER.role_id == 2) {
            db.ref("pelanggan/" + user.id + "/order/").update(order);
          } else {
            db.ref("bengkel/" + user.id + "/order/").update(order);
          }
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            Vue.$toast.open({
              message: err.response.data.message,
              type: "warning",
              position: "bottom",
            });
          }
        });
    },
    GET_ORDER_SEARCH(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/filter/keyword", {
          id: LOGGED_IN_USER.id,
          keyword: payload,
          role_id: LOGGED_IN_USER.role_id,
        })
        .then((res) => {
          state.orders = res.data;
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    GET_ORDER_FILTER(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/filter/status", {
          id: LOGGED_IN_USER.id,
          status: payload,
          role_id: LOGGED_IN_USER.role_id,
        })
        .then((res) => {
          state.orders = res.data;
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
  },
  actions: {
    manual_add_order({ commit }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/store", {
          service: payload.service_id,
          pelanggan: payload.pelanggan_id,
          bengkel_id: LOGGED_IN_USER.id,
          waktu_dimulai: payload.waktu_mulai,
          waktu_selesai: payload.waktu_selesai,
          status: "masuk",
        })
        .then((res) => {
          commit("User/GET_PROFILE");
          commit("GET_ORDER");
          router.push("/home");
          Vue.$toast.open({
            message: "Data pesanan berhasil ditambahkan",
            type: "success",
            position: "bottom",
          });
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    add_order({ commit }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/store", {
          service: payload.service_id,
          pelanggan: payload.pelanggan_id,
          bengkel_id: payload.bengkel_id,
          waktu_dimulai: payload.waktu_mulai,
          waktu_selesai: payload.waktu_selesai,
          status: "masuk",
        })
        .then((res) => {
          commit("User/GET_PROFILE");
          commit("GET_ORDER");
          router.push("/home");
          Vue.$toast.open({
            message: "Data pesanan berhasil ditambahkan",
            type: "success",
            position: "bottom",
          });
          Vue.$toast.open({
            message: "Tunggu status anda hingga berubah",
            type: "warning",
            position: "bottom",
          });
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data.message,
            type: "warning",
            position: "bottom",
          });
        });
    },
    change_status({ commit }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("orders/update/status", {
          status: payload.status,
          id: payload.id,
          user_id: LOGGED_IN_USER.id,
          keterangan: payload.keterangan,
        })
        .then((res) => {
          commit("User/GET_PROFILE");
          commit("GET_DETAIL_ORDER", payload);
          Vue.$toast.open({
            message: "Status pesanan berhasil diganti",
            type: "success",
            position: "bottom",
          });
          router.push("/home");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "error",
            position: "bottom",
          });
        });
    },
  },
  getters: {
    orders(state) {
      return state.orders;
    },
    order(state) {
      return state.order;
    },
    queues(state) {
      return state.queues;
    },
  },
};

export default Order;
