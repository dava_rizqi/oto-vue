import Vue from "vue";
import axios from "axios";
import router from "../../router/index";

const Service = {
  state() {
    return {
      service: {
        waktu_layanan: null,
        harga_layanan: null,
        nama_layanan: null,
      },
      categories: [],
    };
  },
  mutations: {
    SET_SERVICE(state, payload) {
      state.service = payload;
    },
    CLEAR_SERVICE(state, payload) {
      state.service = {
        waktu_layanan: null,
        harga_layanan: null,
        nama_layanan: null,
      };
    },
    GET_SERVICE_CATEGORY(state, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("services/category/get", {
          user_id: LOGGED_IN_USER.id,
        })
        .then((res) => {
          state.categories = res.data;
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
  },
  actions: {
    add_service({ commit }, payload) {
      axios
        .post("services/store", payload)
        .then((res) => {
          commit("User/GET_PROFILE");
          Vue.$toast.open({
            message: "Layanan berhasil ditambahkan",
            type: "success",
            position: "bottom",
          });
          router.push("/view-service");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    add_service_category({ commit }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      axios
        .post("services/category/store", {
          nama_kategori: payload.nama_kategori,
          user_id: LOGGED_IN_USER.id,
        })
        .then((res) => {
          commit("GET_SERVICE_CATEGORY");
          Vue.$toast.open({
            message: "Kategori layanan berhasil ditambahkan",
            type: "success",
            position: "bottom",
          });
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    delete_service_category({ commit }, id) {
      axios
        .post("services/category/delete", {
          id: id,
        })
        .then((res) => {
          commit("GET_SERVICE_CATEGORY");
          Vue.$toast.open({
            message: "Kategori layanan berhasil dihapus",
            type: "success",
            position: "bottom",
          });
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    get_detail_service({ commit }, payload) {
      let LOGGED_IN_USER = JSON.parse(localStorage.getItem("oto_session"));
      if (LOGGED_IN_USER.role_id === 3) {
        var user_id = LOGGED_IN_USER.id;
      } else {
        var user_id = payload.bengkel_id;
      }
      axios
        .post("services/detail", {
          nama_layanan: payload.nama_layanan,
          user_id: user_id,
        })
        .then((res) => {
          console.log(res.data);
          commit("SET_SERVICE", res.data);
          // console.log(res.data)
          // Vue.$toast.open({
          //     message : res.data.waktu_layanan + ' menit',
          //     type : 'success',
          //     position : 'bottom'
          // });
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    update_service({ commit }, payload) {
      axios
        .post("services/update", payload)
        .then((res) => {
          console.log(payload);
          commit("User/GET_PROFILE");
          Vue.$toast.open({
            message: "Layanan berhasil diperbaharui",
            type: "success",
            position: "bottom",
          });
          router.push("/view-service");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
    delete_service({ commit }, payload) {
      axios
        .post("services/delete", payload)
        .then((res) => {
          console.log(payload);
          commit("User/GET_PROFILE");
          Vue.$toast.open({
            message: "Layanan berhasil dihapus",
            type: "success",
            position: "bottom",
          });
          router.push("/view-service");
        })
        .catch((err) => {
          Vue.$toast.open({
            message: err.response.data,
            type: "warning",
            position: "bottom",
          });
        });
    },
  },
  getters: {
    service(state) {
      return state.service;
    },
    categories(state) {
      return state.categories;
    },
  },
};

export default Service;
