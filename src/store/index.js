import Vue from "vue";
import Vuex from "vuex";
import Alert from "./modules/alert";
import Auth from "./modules/auth";
import User from "./modules/user";
import Order from "./modules/order";
import Service from "./modules/service";
import Firebase from "./modules/Firebase";
import Loader from "./modules/Loader";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Loader,
    Alert,
    Auth,
    User,
    Firebase,
    Order,
    Service,
  },
});
